import pygame
import math
from pygments.lexers import ampl
import numpy as np

def scaleImage(image,scale):
    rect = image.get_rect()
    image = pygame.transform.smoothscale(image, tuple((np.array(rect.size)*scale).astype(int)))
    return (image,image.get_rect())

class DrivingCar():
    def __init__(self,asset_path,scale=1,center=(0,0),angle=0,simstep=0.01):
        # Initialize car
        self.car = pygame.image.load(asset_path+"carfull_nowheels_cent.jpg").convert()
        (self.car,self.rect_car) = scaleImage(self.car,scale)
        
        # Initialize wheels
        self.wheel = pygame.image.load(asset_path+"singlewheel.png").convert_alpha()
        (self.wheel,self.rect_wheel) = scaleImage(self.wheel,scale*1.4)
        self.rect_Lwheel = self.rect_wheel.copy()
        self.rect_Rwheel = self.rect_wheel.copy()
        self.surfsize = (self.rect_car.width+2*self.rect_wheel.height,
                                    self.rect_car.height+self.rect_wheel.height)
        self.rect = self.rect_car.copy()
        
        self.rect_car.center = tuple((np.array(self.surfsize)/2).astype(int))
        self.rect_Lwheel.center = (int(self.rect_car.left+120*scale),
                                   int(self.rect_car.top+180*scale))
        self.rect_Rwheel.center = (int(self.rect_car.right-145*scale),
                                   int(self.rect_car.top+180*scale))
        
        self.angle_orig = np.radians(angle)
        self.angle = np.radians(angle)  # Radians
        
        self.pos_x_orig = center[0]
        self.pos_y_orig = center[1] 
        
    
        self.simstep = simstep
        self.Servo = Servo(simstep)
        self.Drive = Drive(simstep)
        
        self.r_vec = np.array([0,-self.rect_car.height/2])
        self.r_beam = np.radians(60)/2
        self.r_dist = 500
        
        self.reset()
        
    def reset(self):
        self.angle = self.angle_orig  # Radians
        self.pos_x = self.pos_x_orig
        self.pos_y = self.pos_y_orig
        self.Servo.reset()
        self.Drive.reset()
        self.rect.center = (200,200)  # Move the car to an empty area so we don't collide immediately      
    
    def draw(self,screen):
        #rotate wheels
        rot_wheel = pygame.transform.rotate(self.wheel,self.Servo.angle)
        rot_rect_Lwheel = rot_wheel.get_rect()
        rot_rect_Lwheel.center = self.rect_Lwheel.center
        rot_rect_Rwheel = rot_wheel.get_rect()
        rot_rect_Rwheel.center = self.rect_Rwheel.center
        
        surf = pygame.Surface(self.surfsize,pygame.SRCALPHA)
        surf.blit(self.car,self.rect_car)
        surf.blit(rot_wheel,rot_rect_Lwheel)
        surf.blit(rot_wheel,rot_rect_Rwheel)
        surf = pygame.transform.rotate(surf,math.degrees(self.angle))
        self.rect = surf.get_rect(center=(self.pos_x,self.pos_y))
        screen.blit(surf,surf.get_rect(center=(self.pos_x,self.pos_y)))
        
    # Equations from Polack et.al., 2017 "The kinematic bicycle model:..."
    def update(self):
        self.Servo.update()
        self.Drive.update()
        c1 = np.arctan(0.5*np.tan(np.radians(self.Servo.angle)))
        self.angle += self.Drive.speed*self.simstep*np.sin(c1)/12
        self.angle = self.angle % (2*np.pi)
        #self.pos_x += self.Drive.speed*np.cos(np.radians(self.Servo.angle)+c1)*self.simstep
        #self.pos_y += self.Drive.speed*np.sin(np.radians(self.Servo.angle)+c1)*self.simstep
        self.pos_y -= self.Drive.speed*np.cos(self.angle+c1)*self.simstep
        self.pos_x -= self.Drive.speed*np.sin(self.angle+c1)*self.simstep
        
    def center(self):
        return (self.pos_x,self.pos_y)
    
    def detectobstacle(self):
#         anglesin = np.sin(self.angle)
#         anglecos = np.cos(self.angle)
#         txmatrix = np.array([[anglecos,anglesin],[-anglesin,anglecos]])
#         r_vec = txmatrix.dot(self.r_vec)
#         r_pos = r_vec + np.array(self.center())
#         
#         r_to_o = np.array(target)-r_pos
#         dist = np.linalg.norm(r_to_o)-radius
#         angle = np.arccos(r_vec.dot(r_to_o)/(np.linalg.norm(r_vec)*np.linalg.norm(r_to_o)))
#         if abs(angle) <= self.r_beam and dist < 490:
#             return int(dist)#+np.random.normal(0,scale=3))
#         return int(np.random.normal(500,scale=10))
        return self.r_dist # Calculated in collidecirc
    
    def collidecirc(self,target=(0,0),radius=10):
    
        #rotate circle around car (so car is straight up and down)
        anglesin = np.sin(-self.angle)
        anglecos = np.cos(-self.angle)
        txmatrix = np.array([[anglecos,anglesin],[-anglesin,anglecos]])
        c_to_o = txmatrix.dot(np.array(target)-np.array(self.center()))
        r_to_o = c_to_o - self.r_vec
        dist = np.linalg.norm(r_to_o)-radius
        angle = np.arccos(self.r_vec.dot(r_to_o)/(np.linalg.norm(self.r_vec)*np.linalg.norm(r_to_o)))
        if abs(angle) <= self.r_beam and dist < 490:
            self.r_dist = int(dist)#+np.random.normal(0,scale=3))
        else:
            self.r_dist = int(np.random.normal(500,scale=10))
        # https://stackoverflow.com/questions/24727773/detecting-rectangle-collision-with-a-circle
        w = self.rect_car.width/2
        h = self.rect_car.height/2
        cleft,ctop = c_to_o[0]-radius,c_to_o[1]-radius
        cright,cbot = c_to_o[0]+radius,c_to_o[1]+radius
        
        
        # reject if bounding boxes do not intersect
        if not (w < cleft or -w > cright or h < ctop or -h > cbot):
            # Detect if circle hits a corner
            for _x in (-w,w):
                for _y in (-h,h):
                    if math.hypot(_x-c_to_o[0],_y-c_to_o[1]) <= radius:
                        return True
            # Detect if circle hits top or bottom
            if c_to_o[0] < w and c_to_o[0] > -w:
                if -h < cbot and h > ctop:
                    return True
            # Detect if circle hits left or right
            if c_to_o[1] < h and c_to_o[1] > -h:
                if w > cleft and -w < cright:
                    return True
                    
        return False
    
    def getangle(self):
        return -self.angle+np.radians(np.random.normal(0,scale=5))        

class Slider():
    def __init__(self,file,val,mini,maxi,pos):
        self.val = val  # start value
        self.maxi = maxi  # maximum at slider position right
        self.mini = mini  # minimum at slider position left
        self.xpos = pos  # x-location on screen
        self.ypos = 100
        self.surf = pygame.surface.Surface((100,600))
        self.hit = False
        
        self.image = pygame.image.load(file).convert()
        #self.image = pygame.transform.scale(self.image,(int(387*.4),int(500*.4)))
        self.image_rect = self.image.get_rect()
        self.image_rect.centerx = self.xpos
        self.image_rect.centery = 100+400*(self.val-self.mini)/(self.maxi-self.mini)
    
    def draw(self,screen):
        screen.blit(self.image,self.image_rect)
        
    def move(self):
        pos = pygame.mouse.get_pos()
        val = (self.maxi-self.mini)*(pos[1] - self.ypos)/400
        self.setval(val)
    
    def setval(self,val):
        self.val = val
        if self.val > self.maxi:
            self.val = self.maxi
        elif self.val < self.mini:
            self.val = self.mini
        self.image_rect.centery = 100+400*(self.val-self.mini)/(self.maxi-self.mini)

class SlideSwitch():
    def __init__(self,file,val,center=(0,0),scale=1,axis='y',labels=True,title='',port=None,pin=None):
        self.axis = axis
        self.val_orig = val
        self.val = val
        self.image = pygame.image.load(file).convert_alpha()
        self.rect = self.image.get_rect()
        self.image = pygame.transform.smoothscale(self.image,(int(self.rect.width*scale),int(self.rect.height*scale)))
        if axis == 'x':
            self.image = pygame.transform.rotate(self.image,-90)
        self.rect = self.image.get_rect()
        self.rect.center = center
        self.labels = labels
        self.title = False
        self.port = port
        self.pin = pin
        font = pygame.font.SysFont('Serif', 18)
        
        if labels:
            self.label0 = font.render('0',True,(0,0,0))
            self.label0_rect = self.label0.get_rect()
            self.label0 = pygame.transform.smoothscale(self.label0,(int(self.label0_rect.width*scale),int(self.label0_rect.height*scale)))
            self.label0_rect = self.label0.get_rect()
            
            self.label1 = font.render('1',True,(0,0,0))
            self.label1_rect = self.label1.get_rect()
            self.label1 = pygame.transform.smoothscale(self.label1,(int(self.label1_rect.width*scale),int(self.label1_rect.height*scale)))
            self.label1_rect = self.label1.get_rect()
            
            if axis == 'y':
                self.label0_rect.centerx = self.rect.centerx
                self.label0_rect.centery = self.rect.centery + 35*scale
                self.label1_rect.centerx = self.rect.centerx
                self.label1_rect.centery = self.rect.centery - 37*scale
            else:
                self.label0_rect.centery = self.rect.centery
                self.label0_rect.centerx = self.rect.centerx - 37*scale
                self.label1_rect.centery = self.rect.centery
                self.label1_rect.centerx = self.rect.centerx + 37*scale
        if title or (self.port is not None and self.pin is not None):
            if not title:
                title = 'P{}.{}'.format(self.port,self.pin)
            self.title = font.render(title,True,(0,0,0))
            self.title_rect = self.title.get_rect()
            self.title = pygame.transform.smoothscale(self.title,(int(self.title_rect.width*scale),int(self.title_rect.height*scale)))
            self.title_rect = self.title.get_rect()
            if axis == 'y':
                self.title_rect.centerx = self.rect.centerx
                self.title_rect.bottom = self.rect.top - 5
            else:
                self.title_rect.right = self.rect.left - 5
                self.title_rect.centery = self.rect.centery
                
    def adjustcenter(self,center=(0,0)):
        dx = center[0]-self.rect.centerx
        dy = center[1]-self.rect.centery
        
        self.rect.centerx = self.rect.centerx+dx
        self.rect.centery = self.rect.centery+dy
        self.label0_rect.centerx = self.label0_rect.centerx+dx
        self.label0_rect.centery = self.label0_rect.centery+dy
        self.label1_rect.centerx = self.label1_rect.centerx+dx
        self.label1_rect.centery = self.label1_rect.centery+dy
        self.title_rect.centerx = self.title_rect.centerx+dx
        self.title_rect.centery = self.title_rect.centery+dy
        
    def reset(self):
        if self.val != self.val_orig:
            self.toggle()
            self.val = self.val_orig
        
    def toggle(self):
        if self.axis == 'y':
            self.image = pygame.transform.flip(self.image,0,1)
        else:
            self.image = pygame.transform.flip(self.image,1,0)
        self.val = not self.val
        
    def settitle(self,title=''):
        self.title = self.font.render(title,True,(0,0,0))
        tr_old = self.title_rect.copy()
        self.title_rect = self.title.get_rect()
        self.title = pygame.transform.smoothscale(self.title,(int(self.title_rect.width*tr_old.height/self.title_rect.height),tr_old.height))
        self.title_rect = self.title.get_rect(center=tr_old.center)
        
    def setcfg(self,port=0,pin=0):
        self.port = port
        self.pin = pin
        if self.title:
            self.settitle('P{}.{}'.format(self.port,self.pin))
    
    def draw(self,screen):
        screen.blit(self.image,self.rect)
        if self.labels:
            screen.blit(self.label0,self.label0_rect)
            screen.blit(self.label1,self.label1_rect)
        if self.title:
            screen.blit(self.title,self.title_rect)

class Pushbutton():
    def __init__(self,file0,file1,val,center=(0,0),scale=1,title='',axis='y',port=None,pin=None):
        self.val_orig = val
        self.val = val
        self.image0 = pygame.image.load(file0).convert_alpha()
        self.image1 = pygame.image.load(file1).convert_alpha()
        self.rect = self.image0.get_rect()
        self.image0 = pygame.transform.smoothscale(self.image0,(int(self.rect.width*scale),int(self.rect.height*scale)))
        self.image1 = pygame.transform.smoothscale(self.image1,(int(self.rect.width*scale),int(self.rect.height*scale)))
        self.rect = self.image0.get_rect()
        self.rect.center = center
        self.title = False
        self.font = pygame.font.SysFont('Serif', 18)
        self.port = port
        self.pin = pin
        
        self.image = self.image0
        
        if title or (self.port is not None and self.pin is not None):
            if not title:
                title = 'P{}.{}'.format(self.port,self.pin)
            self.title = self.font.render(title,True,(0,0,0))
            self.title_rect = self.title.get_rect()
            self.title = pygame.transform.smoothscale(self.title,(int(self.title_rect.width*scale),int(self.title_rect.height*scale)))
            self.title_rect = self.title.get_rect()
            if axis == 'y':
                self.title_rect.centerx = self.rect.centerx
                self.title_rect.bottom = self.rect.top - 5
            else:
                self.title_rect.right = self.rect.left-5
                self.title_rect.centery = self.rect.centery
                
    def adjustcenter(self,center=(0,0)):
        dx = center[0]-self.rect.centerx
        dy = center[1]-self.rect.centery
        
        self.rect.centerx = self.rect.centerx+dx
        self.rect.centery = self.rect.centery+dy
        self.title_rect.centerx = self.title_rect.centerx+dx
        self.title_rect.centery = self.title_rect.centery+dy
        
    def reset(self):
        self.val =self.val_orig
        self.image = self.image0
        
    def press(self):
        self.image = self.image1
        self.val = not self.val_orig
        
    def release(self):
        self.image = self.image0
        self.val = self.val_orig
        
    def settitle(self,title=''):
        self.title = self.font.render(title,True,(0,0,0))
        tr_old = self.title_rect.copy()
        self.title_rect = self.title.get_rect()
        self.title = pygame.transform.smoothscale(self.title,(int(self.title_rect.width*tr_old.height/self.title_rect.height),tr_old.height))
        self.title_rect = self.title.get_rect(center=tr_old.center)
    
    def setcfg(self,port=0,pin=0):
        self.port = port
        self.pin = pin
        if self.title:
            self.settitle('P{}.{}'.format(self.port,self.pin))
        
    def draw(self,screen):
        screen.blit(self.image,self.rect)
        if self.title:
            screen.blit(self.title,self.title_rect)

class Servo():
    def __init__(self,simstep=0.01):    # simstep is update interval in s
        self.desiredangle = 0   # degrees
        self.angle = 0      # degrees
        self.maxturn = 25   # degrees
        self.breakturn = 50
        self.maxchange = 200*simstep   # degrees/second
        
    def reset(self):
        self.angle = 0
        self.desiredangle = 0
    
    # Needs to be called every simulation step
    def update(self):
        offset = self.desiredangle-self.angle
        if offset:
            if abs(offset) > self.maxchange:
                offset = math.copysign(self.maxchange,offset)
            self.angle += offset
        if abs(self.angle) > self.maxturn:
            self.angle = math.copysign(self.maxturn,self.angle)
            
    def set(self,angle=0):
        if abs(angle) > self.maxturn:
            if abs(angle) > self.breakturn:
                self.desiredangle = 0
            else:
                self.desiredangle = math.copysign(self.maxturn,angle)
        else:
            self.desiredangle = angle
            
    def setdc(self,dc=0,per=0):   #pw in seconds
        # If outside of bounds, de-init the motor
        if per < 0.015 or per > 0.025:
            pass
        else:
            pw = dc*per
            self.set((pw-0.0015)/.0005*self.maxturn)
                
                
class Drive():
    def __init__(self,simstep=0.01,loaded=False):
        self.initd = False
        self.initd_count = 0
        self.initd_count_target = 0.9/simstep
        self.inittol = .25
        self.speed = 0
        self.desiredspeed = 10 # cm/s?
        self.maxspeed = 50
        self.breakspeed = 100
        if loaded:
            self.maxchange = 50*simstep
        else:
            self.maxchange = 100*simstep
        
    def reset(self):
        self.initd = False
        self.initd_count = 0
        self.speed = 0
        self.desiredspeed = 10
    
    # Needs to be called every simulation step
    def update(self):
        if self.initd:
            offset = self.desiredspeed-self.speed
            if abs(offset) > self.maxchange:
                offset = math.copysign(self.maxchange,offset)
                self.speed += offset
            else:
                self.speed = self.desiredspeed
        elif abs(self.desiredspeed) < 0.1:
            self.initd_count += 1
            if self.initd_count >= self.initd_count_target:
                self.initd = True
        else:
            self.initd_count = 0
                
    def set(self,speed=0):
        if abs(speed) > self.maxspeed:
            if abs(speed) > self.breakspeed:
                self.desiredspeed = 0
            else:
                self.desiredspeed = math.copysign(self.maxspeed,speed)
        else:
            self.desiredspeed = speed
            
    def setdc(self,dc=0,per=0):   #pw in seconds
        # If outside of bounds, de-init the motor
        if per < 0.015 or per > 0.025:
            self.initd = False 
            self.initd_count = 0
        else:
            pw = dc*per
            self.set((pw-0.0015)/.0004*self.maxspeed)
            
class LED():
    def __init__(self,simstep=0.01):
        self.state = 0      #0 off, 1 on, anything inbetween: brightness
        
    def reset(self):
        self.state = 0
            
    def setdc(self,dc=0,per=0):   #pw in seconds
        if per == 0:
            self.state = 1
        else:
            self.state = 1-dc
        
    def on(self):
        self.state = 1
    
    def off(self):
        self.state = 0
        
    def update(self):
        pass
            
class Obstacle():
    def __init__(self,radius=25,center=(0,0),amp=10,freq=0.5,simstep=0.01,rotate=True):
        self.run = False
        self.centered = True
        self.rotate = rotate
        #self.surf = pygame.Surface((size,size),pygame.SRCALPHA)
        self.rect = pygame.Rect(0,0,radius*2,radius*2)
        self.rect.center = center
        self.rot_rect = self.rect.copy()
        self.radius = radius
        self.amp = amp
        self.freq = 2*np.pi*freq
        self.step = simstep
        self.eyepos = self.rect.center
        
        self.t = 0
        self.direction = np.array([0,0])  # unit vector
        self.angle = 0
        
        self.reset()
        
    def reset(self):
        self.run = False
        self.centered = True
        self.speed = 0
        self.t = 0
        self.direction = 0
        
    def update(self,looktowards=(0,0)):
        # move the obstacle
        if self.run or not self.centered:
            time_dep = np.sin(self.freq*self.t)
            self.rot_rect.center = tuple(self.amp*time_dep*self.direction+np.array(self.rect.center))
            self.t += self.step
            if not self.centered and not self.run:
                if (time_dep > 0) == (self.time_dep_last <= 0): 
                    self.rot_rect.center = self.rect.center
                    self.centered = True
            self.time_dep_last = time_dep
                
        v1 = np.array(looktowards) - np.array(self.rect.center)
        #self.angle = 1.5*np.pi-np.arctan2(v1[1],v1[0])
        self.angle = np.arctan2(v1[1],v1[0])
        self.eyepos = tuple((np.array([self.radius*0.5*np.cos(self.angle),self.radius*0.5*np.sin(self.angle)])+np.array(self.rot_rect.center)).astype(int))
    
    def turnon(self,target=(0,0)):
        self.direction = np.array(target)-np.array(self.rect.center)
        self.direction /= np.linalg.norm(self.direction)
        self.run = True
        self.centered = False
        
    def turnoff(self):
        self.run = False
        
    def draw(self,surf):
        pygame.draw.circle(surf,(0,0,0),self.rot_rect.center,self.radius)
        pygame.draw.circle(surf,(150,90,255),self.rot_rect.center,int(self.radius*.8))
        pygame.draw.circle(surf,(0,0,0),self.eyepos,int(self.radius*.3))
        pygame.draw.circle(surf,(255,255,255),self.eyepos,int(self.radius*.15))
        
        
if __name__ == "__main__":
    import code
    pygame.init()
    screen = pygame.display.set_mode((800,800))
    asset_path = "../assets/"
    car = DrivingCar(asset_path,scale=0.1)
    code.interact(local=dict(globals(), **locals()))
        
        
        
        
        
        